import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
public class Util {

    static List<Contract> listOfSuccessContracts = new ArrayList<>();//содание листа успешных контрактов
    static List<Worker> listSuccessCandidate = new ArrayList<>();
    static List<Contract> listOfUnSuccessContracts = new ArrayList<>();// создание листа не успешных контрактов
    Company firstCompany = new Company();// создание компании
    static void initialiseHeader() {
        System.out.println("");
        Company firstCompany = new Company();
        System.out.println("Enter name company :");////шапка
        Scanner scanner = new Scanner(System.in);
        firstCompany.nameCompany =  scanner.nextLine();
        System.out.println("\n Agenture "+ "\""+firstCompany.nameCompany+"\"");
    }// шапка

    static void initialiseFilials() {
        System.out.println("");////////----Создание трех филлиалов
        System.out.println("--------------------------------Filials-----------------------------------------------------");
        System.out.println("");


        //////// ---- 1---------

        Filials firstFilial= new Filials(); // создание объекта перового филиала
        firstFilial.name = "Symskoi Filial"; // изменяем имя и адресс, номер
        firstFilial.adress = "Symska 24";
        firstFilial.number = 1496559334;
        firstFilial.displayInfo();

        //////// ---- 2---------

        Filials secondFilial= new Filials(); // создание объекта второго филиала
        secondFilial.name = "Izmails Filial"; // изменяем имя и адресс, номер
        secondFilial.adress = "Izmail 2";
        secondFilial.number = 222234557;
        secondFilial.displayInfo();

        //////// ---- 3---------

        Filials thirdFilial= new Filials(); // создание объекта третьего филиала
        thirdFilial.name = "Albishs Filial"; // изменяем имя и адресс, номер
        thirdFilial.adress = "Albish 8";
        thirdFilial.number = 338439394;
        thirdFilial.displayInfo();
    }// филиалы

    static List<Employer> provideListOfEmployers() {

        List<Employer> listOfEmployers = new ArrayList<>();

        System.out.println("");////////----Создание трех работодателей
        System.out.println("-----------------------------Employer---------------------------------------------------");
        System.out.println(" ");


        /////////---- 1---------

        Employer firstEmployer= new Employer(); // создание объекта первого работодателся
        firstEmployer.name = "Artem Olivenko"; // изменяем имя и адресс, работа и номер
        firstEmployer.adress = "city Belzi,    street Kievsky 2      ";
        firstEmployer.vacancy = "Painter";
        firstEmployer.number = 412738290;
        firstEmployer.displayInfo();

        /////////---- 2---------

        Employer secondEmployer= new Employer(); // создание объекта второго работодателся
        secondEmployer.name = "Stepan Bondera"; // изменяем имя и адресс, работа и номер
        secondEmployer.adress = "city Kishinev, street Andrey Doga 3 ";
        secondEmployer.vacancy = "Builder";
        secondEmployer.number = 315543811;
        secondEmployer.displayInfo();

        /////////---- 3---------

        Employer thirdEmployer= new Employer(); // создание объекта первого работодателся
        thirdEmployer.name = "Victor Liashko"; // изменяем имя и адресс, работа и номер
        thirdEmployer.adress = "city Edinet,   street Independence 13";
        thirdEmployer.vacancy = "Director";
        thirdEmployer.number = 532732901;
        thirdEmployer.displayInfo();

        listOfEmployers.add(firstEmployer);
        listOfEmployers.add(secondEmployer);
        listOfEmployers.add(thirdEmployer);
        return listOfEmployers;
    }// лист работодателей

    static List<Candidate> provideListOfCandidate(){
        List<Candidate> listCandidate = new ArrayList<>();

        System.out.println("");//////---Создание трех кандидатов----
        System.out.println("-----------------------------Candidates---------------------------------------------------");
        System.out.println(" ");


        /////////---- 1---------

        Candidate firstCandidate= new Candidate(); // создание объекта первого кандидата
        firstCandidate.name = "You   "; // изменяем имя и адресс, работа и номер
        firstCandidate.adress = "town Konoha,   street Naruto 3       ";
        firstCandidate.position = "Hokage";
        firstCandidate.number = 228549369;
        firstCandidate.displayInfo();

        /////////---- 2---------

        Candidate secondCandidate= new Candidate(); // создание объекта второго кандидата
        secondCandidate.name = "Oleg  "; // изменяем имя и адресс, работа и номер
        secondCandidate.adress = "city Kishinev, avenue Dachia 28       ";
        secondCandidate.position = "Builder";
        secondCandidate.number = 452332390;
        secondCandidate.displayInfo();

        /////////---- 3---------

        Candidate thirdCandidate= new Candidate(); // создание объекта третьего кандидата
        thirdCandidate.name = "Andrey"; // изменяем имя и адресс, работа и номер
        thirdCandidate.adress = "city Tiraspol, st. Karl Liebknecht 1";
        thirdCandidate.position = "Painter";
        thirdCandidate.number = 100352991;
        thirdCandidate.displayInfo();
        listCandidate.add(firstCandidate);
        listCandidate.add(secondCandidate);
        listCandidate.add(thirdCandidate);
        return listCandidate;
    }//лист кандидатов

    static void hiring(Candidate candidate, Employer employer, Scanner scanner) {

        if (candidate.position.equals(employer.vacancy)){ // если по специальности подходит то продолжаем
            System.out.println("\n Please enter pay for hour:-> " + employer.vacancy);
            int oplate = scanner.nextInt();
            System.out.println("Please enter hours: " );
            int timeWork = scanner.nextInt();
            System.out.println("Please enter company fee in procent ^_^ " );
            double kommision = scanner.nextInt();

            String employerName = employer.name;
            String candidateName = candidate.name;
            System.out.println(" \n");

            if (oplate != 0){ // если дает оплату то заключаеться контракт
                double result = oplate * timeWork * kommision/100;
                double salary = oplate * timeWork - (oplate * timeWork * kommision/100);
                Company.earnCompany = Company.earnCompany + result;

               listOfSuccessContracts.add(new Contract(oplate, employer.vacancy, kommision, timeWork,
                       employerName, candidateName));
               listSuccessCandidate.add(new Worker(candidateName, salary));
            }

        }else{ // иначе это не удачный контракт и вакансия не занята
            int oplate = 0;
            int timeWork = 0;
            int kommision = 0;
            String employerName = employer.name;
            String candidateName = candidate.name;
            listOfUnSuccessContracts.add(new Contract(oplate, employer.vacancy, kommision, timeWork,
                    employerName, candidateName));

        }

    }
}
