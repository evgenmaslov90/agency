class Filials { //создание дефолтным конструктором филиалов
    String name;    // название
    String adress;  // адресс
    long number;        // телефон
    String country = "Moldova";

    void displayInfo() {
        System.out.printf("Name: " + name + "\t Adress: " + adress + "\t Country: " + country + "\t Number: " + number + "\n");
    }
}